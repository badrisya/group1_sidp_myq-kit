package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.text.DateFormat;
import java.util.Calendar;


public class NoticationpageActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noticationpage);
      /*  Calendar calender=Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(calender.getTime());
        TextView notificationbox=(TextView)  findViewById(R.id.textView14);


        //TextView notificationbox2=(TextView)  findViewById(R.id.textView15);
       // TextView textViewDate = findViewById(R.id.textView15);


        textViewDate.setText(currentDate);

        StringBuilder stringBuilder = new StringBuilder();
        String notificationchat1 ="Dr James " ;
        String notificationchat2= "Dr Jamess";
        notificationbox.setText(stringBuilder.toString());
               // for (int i =0 ; i<2; i++)
              //  {
                    stringBuilder.append(notificationchat1);
                    //stringBuilder.append(notificationchat2);
              //  }*/

        Button button5 = findViewById(R.id.button5);
        Button button6 = findViewById(R.id.button6);
        Button button7 = findViewById(R.id.button7);
        Button button8 = findViewById(R.id.button8);

        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button5: ;
                openstatuspage();
                break;
            case R.id.button6: ;
//                openstatusmainpage();
            //opensymptomspage();
                openhistorypage();
                break;
            case R.id.button7: ;
                openNoticationpageActivity();
                break;
            case R.id.button8: ;
                openprofilepage();
                break;


        }
    }
    public void openNoticationpageActivity(){
        Intent intent = new Intent(this,NoticationpageActivity.class);
        startActivity(intent);
    }
    public void openprofilepage() {
        Intent intent = new Intent(this, profilepage.class);
        startActivity(intent);
    }
    public void openstatuspage() {
        Intent intent = new Intent(this, statuspage.class);
        startActivity(intent);
    }
    public void openaccountloginpage(){
        Intent intent = new Intent(this,accountloginpage.class);
        startActivity(intent);
    }
    public void opensymptomspage()
    {
        Intent intent= new Intent(this,symptomspage.class);
        startActivity(intent);
    }
    public void openstatusmainpage()
    {
        Intent intent= new Intent(this,Statusmainpage.class);
        startActivity(intent);
    }

    public void openhistorypage(){
        Intent intent = new Intent(this,History2.class);
        startActivity(intent);
    }
}