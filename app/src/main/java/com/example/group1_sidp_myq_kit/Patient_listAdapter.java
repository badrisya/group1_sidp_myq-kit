package com.example.group1_sidp_myq_kit;


import android.content.Context;
        import android.content.Intent;
        import android.view.LayoutInflater;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import androidx.recyclerview.widget.RecyclerView;
        import androidx.annotation.NonNull;
        import android.view.View;
    // import com.example.group1_sidp_myq_kit.Activity.HomeActivity;
        import com.example.group1_sidp_myq_kit.R;
        import com.example.group1_sidp_myq_kit.UserData;
import com.google.firebase.auth.FirebaseAuth;
import org.jetbrains.annotations.NotNull;

        import java.util.ArrayList;

    public class Patient_listAdapter extends RecyclerView.Adapter<com.example.group1_sidp_myq_kit.Patient_listAdapter.MyViewHolder>{
        Context homeActivity;
        ArrayList<PatientData> patientArrayList;

        public Patient_listAdapter(Context context, ArrayList<PatientData> list) {
            this.homeActivity = context;
            this.patientArrayList = list;
        }

        @NonNull

        @Override
        public com.example.group1_sidp_myq_kit.Patient_listAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(homeActivity).inflate(R.layout.item_layout_patientlist,parent,false);
            return new com.example.group1_sidp_myq_kit.Patient_listAdapter.MyViewHolder(view);
        }



        @Override
        public void onBindViewHolder(@NonNull @NotNull com.example.group1_sidp_myq_kit.Patient_listAdapter.MyViewHolder holder, int position) {
            PatientData patientData = patientArrayList.get(position);
           // holder.username.setText(patientData.getUsername());
            holder.heartrate.setText(String.valueOf(patientData.getHeartrate()));
            holder.temp.setText(String.valueOf(patientData.getTemp()));
            holder.spo2.setText(String.valueOf(patientData.getSpo2()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  Intent intent=new Intent(homeActivity, ChatActivity.class); //go to next page
                  //  intent.putExtra("username", patientData.getUsername());
                    //intent.putExtra("userID",userData.getUid());

                   // homeActivity.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return patientArrayList.size();
        }

        public static class MyViewHolder extends RecyclerView.ViewHolder{
            TextView username, heartrate, temp,spo2;
            public MyViewHolder(View itemView){
                super(itemView);
                //username = itemView.findViewById(R.id.user_name);
                heartrate = itemView.findViewById(R.id.heartrate_patientlist);
                temp = itemView.findViewById(R.id.tem_patientlist);
                spo2 = itemView.findViewById(R.id.spo_patientlist);
            }
        }
    }

