package com.example.group1_sidp_myq_kit;

public class ChatData {
    String message;
    private String username;
    private String uid;

    public ChatData(String message, String username, String uid) {
        this.message = message;
        this.username = username;
        this.uid = uid;
    }

    public ChatData() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
