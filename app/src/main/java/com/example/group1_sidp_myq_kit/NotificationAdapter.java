package com.example.group1_sidp_myq_kit;
/*
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class NotificationAdapter {
}*/

        import android.content.Context;
        import android.content.Intent;
        import android.view.LayoutInflater;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import androidx.recyclerview.widget.RecyclerView;
        import androidx.annotation.NonNull;
        import android.view.View;
        // import com.example.group1_sidp_myq_kit.Activity.HomeActivity;
        import com.example.group1_sidp_myq_kit.R;
        import com.example.group1_sidp_myq_kit.UserData;
        import com.google.firebase.auth.FirebaseAuth;
        import org.jetbrains.annotations.NotNull;

        import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>{
    Context homeActivity;
    ArrayList<ChatData> usersArrayList;

    public NotificationAdapter(Context context, ArrayList<ChatData> list) {
        this.homeActivity = context;
        this.usersArrayList = list;
    }

    @NonNull

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.item_notiuser_row,parent,false);
        return new MyViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull @NotNull NotificationAdapter.MyViewHolder holder, int position) {
        ChatData chatData = usersArrayList.get(position);
        holder.username.setText(chatData.getUsername());
        holder.message.setText(chatData.getMessage());
       // holder.phone.setText(chatData.getPhone());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(homeActivity, ChatActivity2.class); //go to next page
                intent.putExtra("username", chatData.getUsername());
                intent.putExtra("message",chatData.getMessage());

                homeActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView username, message ;
        public MyViewHolder(View itemView){
            super(itemView);
            username = itemView.findViewById(R.id.doctornamereceive);
            message = itemView.findViewById(R.id.mesagedetails);
           // phone = itemView.findViewById(R.id.tvphonelaterchangetoheartrate);
        }
    }


}