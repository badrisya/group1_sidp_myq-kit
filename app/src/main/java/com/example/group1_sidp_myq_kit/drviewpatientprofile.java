package com.example.group1_sidp_myq_kit;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

@RequiresApi(api = Build.VERSION_CODES.N)
public class drviewpatientprofile extends AppCompatActivity {

    DatabaseReference databaseReference,dbref2;
    FirebaseUser  user;
    UserData userData;

    String hr = "fail1";
    String snap = "fail2";
    String snapval = "fail3";

    private static final DecimalFormat df = new DecimalFormat("0.00");
    private static final DecimalFormat df2 = new DecimalFormat("#");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drviewpatientprofile);

        databaseReference = FirebaseDatabase.getInstance().getReference("UserData");
        dbref2 = FirebaseDatabase.getInstance().getReference("ReadingData");

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

            for (DataSnapshot dataSnapshot : snapshot.getChildren()){

                if(String.valueOf(dataSnapshot.child("role").getValue()).equals("patient")) {
                    String device = dataSnapshot.child("device").getValue(String.class);
                    String patname = dataSnapshot.child("username").getValue(String.class);

                    Query query = dbref2.child(device).orderByKey().limitToLast(1);
                    DatabaseReference databaseReference1 = dbref2.child(device);

                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                            for (DataSnapshot dataSnapshot1:snapshot.getChildren()){
                                ReadingData readingData = dataSnapshot1.getValue(ReadingData.class);

                                double hr = readingData.getHeartrate();
                                double spo = readingData.getSpo2();
                                double temp = readingData.getTemp();

                                String hrtxt = String.valueOf(df2.format(hr));
                                String spotxt = String.valueOf(df.format(spo));
                                String temptxt = String.valueOf(df.format(temp));
                                String day = readingData.getDate();

                                createTxtView(patname,hrtxt,spotxt,temptxt,day);

                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                    //createTxtView(patname,hr,snap,snapval);
                }

            }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void createTxtView(String username, String hr, String spo, String temp, String day){
        LinearLayout scrollview = (LinearLayout) findViewById(R.id.linscroll);
        TextView text = new TextView(this);
        text.setText("\n  Username: " + username + "  \n" + "  Heart Rate: " + hr + "  \n" + "  SPO2 Reading: " + spo + "  \n" + "  Temperature Reading: " + temp + "  \n" + "  Date: " + day + "  \n");
        text.setTextColor(this.getResources().getColor(R.color.black));
        text.setBackgroundResource(R.drawable.drviewpatshape);
        scrollview.addView(text);
    }

//    public void StringIndexOutOfBoundsException(String s){
//
//    }

}