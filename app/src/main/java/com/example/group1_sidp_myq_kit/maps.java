package com.example.group1_sidp_myq_kit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class maps extends AppCompatActivity {

    private static final int PERMISSIONS_FINE_LOCATION = 99;


    TextView latitude, longitude, altitude, address, updateGps, gpsWifi, updateAddress;
    Switch sw_locationUpdate, sw_gps;

    boolean updateOn = false;

    LocationRequest locationRequest;

    LocationCallback locationCallBack;


    FusedLocationProviderClient fusedLocationProviderClient;

    DatabaseReference databaseReference;
    FirebaseUser user;
    String userid;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        latitude = findViewById(R.id.latitude);
        longitude = findViewById(R.id.logitude);
        altitude = findViewById(R.id.altitude);
        address = findViewById(R.id.address);
        sw_locationUpdate = findViewById(R.id.sw_locationupdate);
        sw_gps = findViewById(R.id.sw_gps);
        updateGps = findViewById(R.id.updateGps);
        gpsWifi = findViewById(R.id.gpsWifi);
        updateAddress = findViewById(R.id.updateAddress);


        locationRequest = new LocationRequest();

        locationRequest.setInterval(3000);

        locationRequest.setFastestInterval(5000);

        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        locationCallBack = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);

                updateValue(locationResult.getLastLocation());
            }
        };

        sw_gps.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (sw_gps.isChecked()) {
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    gpsWifi.setText("Using GPS");
                } else {
                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    gpsWifi.setText("Using Tower and Wifi");
                }
            }
        });


        sw_locationUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sw_locationUpdate.isChecked()){
                    startLocationUpdate();
                }
                else {
                    stopLocationUpdate();
                }
            }
        });

        updateGPS();

    }

    private void stopLocationUpdate() {

        updateGps.setText("Location is NOT tracked");
        latitude.setText("Not tracking location");
        longitude.setText("Not tracking location");
        altitude.setText("Not tracking location");

        fusedLocationProviderClient.removeLocationUpdates(locationCallBack);
    }

    private void startLocationUpdate() {
        updateGps.setText("Location is being tracked");
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallBack, null);
        updateGPS();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSIONS_FINE_LOCATION:
                if (grantResults.length == 0 || grantResults == null) {
                    // show dialog that you need access to go ahead
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Your code here permission granted
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // show dialog that you need access to go ahead
                }
        }
    }

    private void updateGPS() {

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(maps.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        // we got permissions, put the values of location into the UI components
                        updateValue(location);
                        //Location currentLocation = location;
                    }

                }
            });

        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_FINE_LOCATION);

            }
        }

    }


    private void updateValue(Location location) {


        latitude.setText(String.valueOf(location.getLatitude()));
        longitude.setText(String.valueOf(location.getLongitude()));

        if (location.hasAltitude()) {
            altitude.setText(String.valueOf(location.getAltitude()));
        } else {
            altitude.setText("Not available");
        }

        Geocoder geocoder = new Geocoder(maps.this);

        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            updateAddress.setText(addresses.get(0).getAddressLine(0));

            user = FirebaseAuth.getInstance().getCurrentUser();
            databaseReference = FirebaseDatabase.getInstance().getReference("UserData");
            userid = user.getUid();
            userData = new UserData();

            String loc = userData.setAddress(addresses.get(0).getAddressLine(0));
            databaseReference.child(user.getUid()).child("address").setValue(loc);

        }

        catch (Exception e) {
            updateAddress.setText("U to get address");
        }

    }
}




