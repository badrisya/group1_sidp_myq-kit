package com.example.group1_sidp_myq_kit;
/*
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    FirebaseAuth auth;
    RecyclerView mainUserRecyclerView;
    UserAdapter adapter;
    FirebaseDatabase database;
    ArrayList<UserData> usersArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.group1_sidp_myq_kit.R.layout.activity_home);

        auth = FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();

        DatabaseReference reference=database.getReference().child("UserData");
        usersArrayList= new ArrayList<>();


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for( DataSnapshot dataSnapshot:snapshot.getChildren())
                {
                    UserData userData=dataSnapshot.getValue(UserData.class);
                    usersArrayList.add(userData);
                    mainUserRecyclerView.setAdapter(adapter);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mainUserRecyclerView=findViewById(R.id.mainUserRecyclerView);
        mainUserRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter=new UserAdapter(HomeActivity.this,usersArrayList);
      //  mainUserRecyclerView.setAdapter(adapter);




        if(auth.getCurrentUser() == null){
            startActivity(new Intent(HomeActivity.this, signuppage.class)); //class not sure


        }











    }
}
*/
//package com.example.group1_sidp_myq_kit.Activity;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.group1_sidp_myq_kit.UserAdapter;
import com.example.group1_sidp_myq_kit.R;
import com.example.group1_sidp_myq_kit.UserData;
import com.example.group1_sidp_myq_kit.signuppage;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    //DatabaseReference databaseReference2;
    FirebaseDatabase database;
    UserAdapter myAdapter;
    ArrayList<UserData> list;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        auth = FirebaseAuth.getInstance();
      
        databaseReference = FirebaseDatabase.getInstance().getReference("UserData");
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference("uid");



        list= new ArrayList<>();


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {

                    if(String.valueOf(dataSnapshot.child("role").getValue()).equals("patient")) {
                        UserData userData = dataSnapshot.getValue(UserData.class);
                        list.add(userData);
                    }

                }
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {

            }
        });
        databaseReference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {

                    UserData userData = dataSnapshot.getValue(UserData.class);
                    list.add(userData);

                }
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {

            }
        });
        //recyclerView.setHasFixedSize(true);
        recyclerView= findViewById(R.id.mainUserRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter= new UserAdapter(this,list);
        recyclerView.setAdapter(myAdapter);


        if(auth.getCurrentUser() == null){
            startActivity(new Intent(HomeActivity.this, signuppage.class));
        }
    }
}