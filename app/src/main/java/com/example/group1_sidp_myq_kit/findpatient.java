package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.chip.Chip;
import android.widget.Toast;
public class findpatient extends AppCompatActivity {
    //
    public static final String EXTRA_TEXT = "com.example.group1_sidp_myq_kit.EXTRA_TEXT";
    public static int color;
    //
    Chip chip14;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findpatient);

        chip14 = findViewById(R.id.chip14);


        chip14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(findpatient.this, "Added Successfully", Toast.LENGTH_SHORT).show();
                openfindpatient2();

            }
        });


    }
    public void openfindpatient2() {
        //try display edit text another page
        EditText findpatient = (EditText) findViewById(R.id.editTextfindPatient);
        String text=findpatient.getText().toString();
        findpatient.setTextColor(color);
        //till here
        Intent intent = new Intent(this, findpatient2.class);

        intent.putExtra(EXTRA_TEXT,text);
       //
        startActivity(intent);
    }
}