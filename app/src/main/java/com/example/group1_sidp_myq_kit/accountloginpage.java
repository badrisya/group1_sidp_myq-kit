package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class accountloginpage extends AppCompatActivity {
Chip chip6;
Chip chip7;
FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountloginpage);
        chip6 = findViewById(R.id.chip6);
        chip7 = findViewById(R.id.chip7);



        chip6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                openMainActivity();

            }
        });

       firebaseAuth = FirebaseAuth.getInstance();
        chip7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                //openprofilepage();

                TextView email =(TextView) findViewById(R.id.editTextTextPersonName3); //email
                TextView password = (TextView) findViewById(R.id.editTextTextPassword); //password

                String emaildb = email.getText().toString().trim();
                String passdb = password.getText().toString().trim();

                firebaseAuth.signInWithEmailAndPassword(emaildb,passdb).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                        Toast.makeText(accountloginpage.this, "Successful",Toast.LENGTH_SHORT).show();
                        openprofilepage();
                    } else {
                        Toast.makeText(accountloginpage.this, "Incorrect",Toast.LENGTH_SHORT).show();
                    }
                    }
                });
            }
        });
    }
    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void openprofilepage() {
        Intent intent = new Intent(this, profilepage.class);
        startActivity(intent);
    }
}