package com.example.group1_sidp_myq_kit;

import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

@RequiresApi(api = Build.VERSION_CODES.N)
public class History2 extends AppCompatActivity implements View.OnClickListener{

    final int N = 10; // total number of textviews to add
    DatabaseReference databaseReference, dbref2;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;

    String userid,day2;
    String deviceid2 = "";
    String spo = "fail";
    double day = 0;
    double spoavg1,hravg1,tempavg1,sposum,hrsum,tempsum;

    private static final DecimalFormat df = new DecimalFormat("0.00");
    private static final DecimalFormat df2 = new DecimalFormat("#");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history2);

        Button button17 = findViewById(R.id.button17);
        Button button18 = findViewById(R.id.button18);
        Button button19 = findViewById(R.id.button19);
        Button button20 = findViewById(R.id.button20);
        button17.setOnClickListener(this);
        button18.setOnClickListener(this);
        button19.setOnClickListener(this);
        button20.setOnClickListener(this);
        user = FirebaseAuth.getInstance().getCurrentUser();
        userid = user.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("UserData");

        databaseReference.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);

                if (userData != null) {
                        dbref2 = FirebaseDatabase.getInstance().getReference("ReadingData").child(userData.getDevice());
                        dbref2.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {

                                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                                    ReadingData readingData = dataSnapshot.getValue(ReadingData.class);

                                    if (readingData != null){
                                        double hrdb = readingData.getHeartrate();
                                        double spo = readingData.getSpo2();
                                        double temp = readingData.getTemp();

                                        String date = readingData.getDate();

                                        day++;
                                        sposum = sposum + spo;
                                        hrsum = hrsum + hrdb;
                                        tempsum = tempsum + temp;

                                        TextView avgspo = (TextView) findViewById(R.id.spoavg);
                                        TextView avghr = (TextView) findViewById(R.id.hravg);
                                        TextView avgtemp = (TextView) findViewById(R.id.tempavg);

                                        spoavg1 = sposum/day;
                                        hravg1 = hrsum/day;
                                        tempavg1 = tempsum/day;

                                        avgspo.setText(String.valueOf(df.format(spoavg1)));
                                        avghr.setText(String.valueOf(df2.format(hravg1)));
                                        avgtemp.setText(String.valueOf(df.format(tempavg1)));

                                        createTextView(String.valueOf(df2.format(hrdb)), String.valueOf(spo), String.valueOf(df.format(temp)));
                                        createTextDate(date);
                                    }

                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                Toast.makeText(History2.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
                            }
                        });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(History2.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button17: ;
                openstatuspage();
                break;
            case R.id.button18: ;
                //openstatusmainpage();
                openhistorypage();
                break;
            case R.id.button19:
                openhomeactivity();
                //openchatlist();
                //openNoticationpageActivity();
                break;
            case R.id.button20:
                openprofilepage();
                break;
          /*  case R.id.button: ;
                openeditprofilepage();
                break;*/


        }
    }


    //  });

    public void openNoticationpageActivity(){
        Intent intent = new Intent(this,NoticationpageActivity.class);
        startActivity(intent);
    }
    public void openprofilepage() {
        Intent intent = new Intent(this, profilepage.class);
        startActivity(intent);
    }
    public void openstatuspage() {
        Intent intent = new Intent(this, statuspage.class);
        startActivity(intent);
    }
    public void openeditprofilepage(){
        Intent intent = new Intent(this,edit_profilepage.class);
        startActivity(intent);
    }
    public void openhomeactivity()
    {
        Intent intent= new Intent(this, ChatActivity2.class);
        startActivity(intent);
    }
    /* public void openUserFragment()
     {
         Intent intent= new Intent(this, UsersFragment.class);
         startActivity(intent);
     }*/
    public void openstatusmainpage()
    {
        Intent intent= new Intent(this,Statusmainpage.class);
        startActivity(intent);
    }

    public void openhistorypage(){
        Intent intent = new Intent(this,History2.class);
        startActivity(intent);
    }

    public void createTextView(String hrate, String spo2, String temp){
        //heart rate
        LinearLayout hrlin = (LinearLayout) findViewById(R.id.linlay);
        TextView hr = new TextView(this);
        hr.setTextColor(this.getResources().getColor(R.color.black));
        hr.setText(hrate);
        hrlin.addView(hr);

        //spo2
        LinearLayout spolin= (LinearLayout) findViewById(R.id.spolay);
        TextView spo = new TextView(this);
        spo.setText(spo2);
        spolin.addView(spo);

        //temp
        LinearLayout templin= (LinearLayout) findViewById(R.id.templay);
        TextView tempt = new TextView(this);
        tempt.setText(temp);
        templin.addView(tempt);
    }

    public void createTextDate(String date){
        //heart rate
        LinearLayout hrdate = (LinearLayout) findViewById(R.id.hrdate);
        TextView datehr = new TextView(this);
        datehr.setText(date);
        hrdate.addView(datehr);

        //spo2
        LinearLayout spodate = (LinearLayout) findViewById(R.id.spodate);
        TextView spohr = new TextView(this);
        spohr.setText(date);
        spodate.addView(spohr);

        //temp
        LinearLayout tempdate = (LinearLayout) findViewById(R.id.tempdate);
        TextView datetemp = new TextView(this);
        datetemp.setText(date);
        tempdate.addView(datetemp);
    }

}