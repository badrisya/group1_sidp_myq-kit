package com.example.group1_sidp_myq_kit;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;

public class ChatActivity2 extends AppCompatActivity implements View.OnClickListener{

    DatabaseReference databaseReference, dbref2;
    FirebaseUser user, druser, patuser;
    UserData userData;

    String dr, pat,mess,patid;
    String message2 = "fail";

//    Messages msg;
    Date date;
    long date2;

    public static final String CHANNEL_1 = "channel1";
    private NotificationManagerCompat notificationManager;

//    MessageAdater adater;
//    RecyclerView messageAdater;
//    ArrayList<Messages> messagesArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat3);

        CardView sendbtn = findViewById(R.id.sendBtn);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("UserData").child(user.getUid());
        pat = getIntent().getStringExtra("username");
        notificationManager = NotificationManagerCompat.from(this);
        dbref2 = FirebaseDatabase.getInstance().getReference("Chat");

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.child("role").getValue(String.class).equals("doctor")){

                    //msg = new Messages();
                    EditText message = (EditText) findViewById(R.id.edtMessage);
                    TextView txtMessages = (TextView) findViewById(R.id.txtMessages);

                    dbref2.child(pat).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                            for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                                    mess = dataSnapshot.child("message").getValue(String.class);
                                }
                                createTextView(mess);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                    sendbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            date = new Date();
                            message2 = message.getText().toString();
                            date2 = date.getTime();
                            dbref2.child(pat).child(String.valueOf(date2)).child("message").setValue(message2);
                            dbref2.child(pat).child(String.valueOf(date2)).child("sid").setValue(user.getUid());
                            message.getText().clear();
                            createTextView(message2);
                            createNotification();

                        }
                    });

                } else if(snapshot.child("role").getValue(String.class).equals("patient")){

                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            UserData userData = snapshot.getValue(UserData.class);
                            patid = userData.getUsername();

                            dbref2.child(patid).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {

                                    for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                                            mess = dataSnapshot.child("message").getValue(String.class);
                                        }
                                        createTextView(mess);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void createNotification(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            NotificationChannel channel1 = new NotificationChannel(CHANNEL_1, "Channel 1", NotificationManager.IMPORTANCE_DEFAULT);
            channel1.setDescription("MyQkit");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);

            Intent resultIntent = new Intent(this,accountloginpage.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);

            Notification notification = new NotificationCompat.Builder(this,CHANNEL_1)
                    .setSmallIcon((R.drawable.ic_android_black_24dp))
                    .setContentTitle(" Message from Doctor ")
                    .setContentText("Login to see the messages")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)

                    .setContentIntent(resultPendingIntent)

                    .build();
            notificationManager.notify(1,notification);

        }
    }








    public void createTextView(String messagetxt){
        LinearLayout scrollView = (LinearLayout) findViewById(R.id.linscroll);
        TextView text = new TextView(this);
        text.setText("  " + messagetxt + "  ");
        text.setTextSize(20);
        text.setTextColor(this.getResources().getColor(R.color.black));
        text.setBackgroundResource(R.drawable.sender_shape);
        scrollView.addView(text);

    }

    @Override
    public void onClick(View view) {

    }
}
