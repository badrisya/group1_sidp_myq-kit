package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class doctorlogin extends AppCompatActivity {
Chip chip8;
Chip chip9;
FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctorlogin);
        chip8 = findViewById(R.id.chip8);
        chip9 = findViewById(R.id.chip9);

        chip8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                openMainActivity();


            }
        });
        firebaseAuth = FirebaseAuth.getInstance();
        chip9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView email =(TextView) findViewById(R.id.editTextTextPersonName2); //email
                TextView password = (TextView) findViewById(R.id.editTextTextPassword2); //password

                String emaildb = email.getText().toString().trim();
                String passdb = password.getText().toString().trim();

                firebaseAuth.signInWithEmailAndPassword(emaildb,passdb).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(doctorlogin.this, "Successful",Toast.LENGTH_SHORT).show();
                            opendoctorprofilepage();
                        } else {
                            Toast.makeText(doctorlogin.this, "Incorrect",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void opendoctorprofilepage(){
        Intent intent = new Intent(this,doctorprofilepage.class);
        startActivity(intent);
    }
}