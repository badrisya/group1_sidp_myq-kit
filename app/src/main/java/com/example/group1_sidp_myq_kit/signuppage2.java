package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class signuppage2 extends AppCompatActivity {
Chip chip12;
Chip chip13;

     String username = "";
     String password = "";
     String role = "";
     String ic = "";
     String dob = "";
     String gender = "";
     String height = "";
     String weight = "";
     String phone = "";
     String age = "";
     String address = "";
     int p;

UserData userData;
DatabaseReference databaseReference;

    public signuppage2() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signuppage2);
        chip12 = findViewById(R.id.chip12);
        chip13 = findViewById(R.id.chip13);

        chip12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                opensignuppage();

            }
        });

        userData = new UserData();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("UserData");
        chip13.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                p++;
                username = userData.getUsername();
                //Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                TextView email =(TextView) findViewById(R.id.editTextTextEmailAddress); //email
                TextView password = (TextView) findViewById(R.id.editTextTextPassword3); //password
                TextView conpass = (TextView)findViewById(R.id.editTextTextPassword5); //confirm password

                String emaildb = email.getText().toString();
                String passdb = password.getText().toString();
                String confpassdb = conpass.getText().toString();

               // databaseReference.child(userData.getUsername()).child("email").setValue(emaildb);
               // databaseReference.child("work").child("password").setValue(passdb);
                //databaseReference.child("bad").child("test1").setValue(emaildb);
               // databaseReference.child("test").child("test2").setValue(databaseReference.child("username").limitToLast(1));
                //databaseReference.child("username").limitToLast(1);



               //databaseReference.child("test").setValue(userData);
                opensuccessRegristrationPage();
            }
        });
    }
    public void opensignuppage () {
        Intent intent = new Intent(this, signuppage.class);
        startActivity(intent);
    }
    public void opensuccessRegristrationPage () {
        Intent intent = new Intent(this, successRegristrationPage.class);
        startActivity(intent);
    }

}