package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.chip.Chip;

public class edit_profilepage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profilepage);

        Chip chip13 = findViewById(R.id.chip13);

        chip13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(edit_profilepage.this, "Saved", Toast.LENGTH_SHORT).show();
                openprofilepage();

            }
        });
    }
    public void openprofilepage() {
        Intent intent = new Intent(this, profilepage.class);
        startActivity(intent);
    }

}