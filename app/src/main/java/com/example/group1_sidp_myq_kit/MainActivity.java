package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.chip.Chip;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
 private Button button;
 Chip chip1;
 Chip chip5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chip1 = findViewById(R.id.chip4);
        chip5 =findViewById(R.id.chip5);
       button=(Button) findViewById(R.id.button);


        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                openOptionsignuppage();

            }
        });
        chip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                opendoctorlogin();
            }
        });
        chip5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                openaccountloginpage();
            }
        });


    }
    public void open(){
       Intent intent = new Intent(this,signuppage.class);
       startActivity(intent);
    }
    public void openaccountloginpage(){
        Intent intent = new Intent(this,accountloginpage.class);
        startActivity(intent);
    }
    public void opendoctorlogin() {
        Intent intent = new Intent(this, doctorlogin.class);
        startActivity(intent);
    }
    public void openOptionsignuppage(){
        Intent intent= new Intent(this,Optionsignuppage.class);
        startActivity(intent);

    }
}