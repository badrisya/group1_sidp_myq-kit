package com.example.group1_sidp_myq_kit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MessageAdater extends RecyclerView.Adapter {

    Context context;
    ArrayList<Messages> messagesArrayList;
    int ITEM_SEND=1;
   int ITEM_RECIVE=2;

    public MessageAdater(Context context, ArrayList<Messages> messagesArrayList) {
        this.context = context;
        this.messagesArrayList = messagesArrayList;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( @NotNull ViewGroup parent, int viewType) {


        if(viewType==ITEM_SEND)
        {
            View view = LayoutInflater.from(context).inflate(R.layout.sender_layout_item,parent,false);
            return new SenderViewHolder(view);
        }
       /* else
        {
            View view = LayoutInflater.from(context).inflate(R.layout.reciver_layout_item,parent,false);
            return new ReciverViewHolder(view);
        }*/
       return null;
    }




    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
    Messages messages= messagesArrayList.get(position);
    if(holder.getClass()==SenderViewHolder.class)
    {
        SenderViewHolder viewHolder =(SenderViewHolder) holder;

        viewHolder.txtmessage.setText(messages.getMessage());
    }/*else
    {
        ReciverViewHolder viewHolder =(ReciverViewHolder) holder;

        viewHolder.txtmessage.setText(messages.getMessage());
    }*/

    }

    @Override
    public int getItemCount() {
        return messagesArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
       Messages messages= messagesArrayList.get(position);

       if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(messages.getSenderId()))
       {
           return ITEM_SEND;
       }else{
           return ITEM_RECIVE;
       }
    }

    class SenderViewHolder extends RecyclerView.ViewHolder{

        TextView txtmessage;

        public SenderViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtmessage= itemView.findViewById(R.id.txtMessages);
        }
    }
   /* class ReciverViewHolder extends RecyclerView.ViewHolder{

        TextView txtmessage;

        public ReciverViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtmessage= itemView.findViewById(R.id.txtMessages);
        }
    }*/
}
