package com.example.group1_sidp_myq_kit;

public class UserData {
    private String username;
    private String password;
    private String role;
    private String ic;
    private String dob;
    private String gender;
    private String height;
    private String weight;
    private String phone;
    private String age;
    private String address;
    private String email;
    private String uid;
    private String workingplace; //new
    private String specialist;

    String x;
    int p;
    private String device;
    double heartrate,spo2,temp;

    public UserData() {

    }


/* public UserData(String username, String password, String role, String ic, String dob, String gender, String height, String weight, String phone, String age, String address, String email, String uid)
    {
        this.username = username;
        this.password = password  ;
        this.role = role;
        this.ic = ic;
        this.dob = dob;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.phone = phone;
        this.age = age;
        this.address = address;
        this.email = email;
        this.uid = uid;

    }*/

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() { return phone; }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void store(String z){
        x = z;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getWorkingPlace() {return workingplace;}

    public void setWorkingPlace(String workingPlace) {this.workingplace = workingPlace;}

    public String getSpecialist() {return specialist;}

    public void setSpecialist(String specialist) {this.specialist = specialist;}

    public String returnstore(String y){
        y = x;
        return y;
    }

    public String test(int p){
        return "fromuserdata"+p;
    }

    public double getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(double heartrate) {
        this.heartrate = heartrate;
    }

    public double getSpo2() {
        return spo2;
    }

    public void setSpo2(double spo2) {
        this.spo2 = spo2;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }
}
