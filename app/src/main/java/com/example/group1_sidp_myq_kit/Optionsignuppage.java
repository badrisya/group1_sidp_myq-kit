package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.chip.Chip;

public class Optionsignuppage extends AppCompatActivity {
    Chip chip16;//patient
    Chip chip15;//dr
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionsignuppage);

        chip15 =findViewById(R.id.chip15);
        chip16 = findViewById(R.id.chip16);

        chip15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                opendocsignuppage();
            }
        });
        chip16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                opensignuppage();
            }
        });
    }


    public void opensignuppage() {
        Intent intent = new Intent(this, signuppage.class);
        startActivity(intent);
    }
    public void opendocsignuppage() {
        Intent intent = new Intent(this, docsignuppage.class);
        startActivity(intent);
    }
}