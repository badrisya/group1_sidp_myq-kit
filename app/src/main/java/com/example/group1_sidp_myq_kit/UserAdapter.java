package com.example.group1_sidp_myq_kit;
/*
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.Viewholder> {

    Context homeActivity;
    ArrayList<UserData> usersArrayList;

    public UserAdapter(HomeActivity homeActivity, ArrayList<UserData> usersArrayList) {
        this.homeActivity=homeActivity;
        this.usersArrayList=usersArrayList;
    }

    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(homeActivity).inflate(R.layout.item_user_row,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserAdapter.Viewholder holder, int position) {
      UserData userData = usersArrayList.get(position);

      holder.user_name.setText(userData.getUsername());
        holder.user_ic.setText(userData.getIc());
        holder.user_phone.setText(userData.getPhone());

    }


    @Override
    public int getItemCount() {

        return usersArrayList.size();
    }

    class Viewholder extends RecyclerView.ViewHolder{
        TextView user_name;
        TextView user_ic;
        TextView user_phone;

     public Viewholder(@NonNull View itemView)
     {
         super((itemView));


     }
    }
}
*/
import android.content.Context;
        import android.content.Intent;
        import android.view.LayoutInflater;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import androidx.recyclerview.widget.RecyclerView;
        import androidx.annotation.NonNull;
        import android.view.View;
       // import com.example.group1_sidp_myq_kit.Activity.HomeActivity;
        import com.example.group1_sidp_myq_kit.R;
        import com.example.group1_sidp_myq_kit.UserData;
import com.google.firebase.auth.FirebaseAuth;
import org.jetbrains.annotations.NotNull;

        import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder>{
    Context homeActivity;
    ArrayList<UserData> usersArrayList;

    public UserAdapter(Context context, ArrayList<UserData> list) {
        this.homeActivity = context;
        this.usersArrayList = list;
    }

    @NonNull

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.item_user_row,parent,false);
        return new MyViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull @NotNull UserAdapter.MyViewHolder holder, int position) {
        UserData userData = usersArrayList.get(position);
        holder.username.setText(userData.getUsername());
        holder.ic.setText(userData.getIc());
        holder.phone.setText(userData.getPhone());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(homeActivity, ChatActivity2.class); //go to next page
                intent.putExtra("username", userData.getUsername());
                //intent.putExtra("userID",userData.getUid());

                homeActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView username, ic , phone;
        public MyViewHolder(View itemView){
            super(itemView);
            username = itemView.findViewById(R.id.user_name);
           ic = itemView.findViewById(R.id.iclaterchangetotemp);
            phone = itemView.findViewById(R.id.tvphonelaterchangetoheartrate);
        }
    }
}