package com.example.group1_sidp_myq_kit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.auth.FirebaseAuth;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MessageAdapterReceive extends RecyclerView.Adapter {
    Context context;
    ArrayList<Messages> messagesArrayList2;
    int ITEM_SEND = 2;
    int ITEM_RECIVE = 1;

    public MessageAdapterReceive(Context context, ArrayList<Messages> messagesArrayList2) {
        this.context = context;
        this.messagesArrayList2 = messagesArrayList2;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {


        if (viewType == ITEM_RECIVE) {
            View view = LayoutInflater.from(context).inflate(R.layout.reciver_layout_item, parent, false);
            return new MessageAdapterReceive.ReciverViewHolder(view);
        }

        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        Messages messages = messagesArrayList2.get(position);
        if (holder.getClass() == MessageAdapterReceive.ReciverViewHolder.class) {
            MessageAdapterReceive.ReciverViewHolder viewHolder = (MessageAdapterReceive.ReciverViewHolder) holder;

            viewHolder.txtmessage.setText(messages.getMessage());
        }/*else
    {
        ReciverViewHolder viewHolder =(ReciverViewHolder) holder;

        viewHolder.txtmessage.setText(messages.getMessage());
    }*/

    }

    @Override
    public int getItemCount() {
        return messagesArrayList2.size();
    }

    @Override
    public int getItemViewType(int position) {
        Messages messages = messagesArrayList2.get(position);

        if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(messages.getSenderId())) {
            return ITEM_RECIVE;
        } else {
            return ITEM_RECIVE;
        }
    }

  /*  class SenderViewHolder extends RecyclerView.ViewHolder {

        TextView txtmessage;

        public SenderViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtmessage = itemView.findViewById(R.id.txtMessages);
        }
    }*/

    class ReciverViewHolder extends RecyclerView.ViewHolder {

        TextView txtmessage;

        public ReciverViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtmessage = itemView.findViewById(R.id.txtMessages);
        }
    }
}



