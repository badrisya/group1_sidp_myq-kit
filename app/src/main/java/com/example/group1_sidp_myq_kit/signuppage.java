package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.sql.SQLException;

public class signuppage extends AppCompatActivity {
    String roledb = "patient";
    Chip chip10;
    Chip chip11;
    String namestore;

    UserData userData;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser user1;
    ReadingData readingData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signuppage);


 //       chip10 = findViewById(R.id.chip10);
        chip11 = findViewById(R.id.chip11);

//        chip10.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {//Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
//                openprofilepage();
//
//            }
//        });

        userData = new UserData();
        readingData = new ReadingData();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("UserData");
        firebaseAuth = FirebaseAuth.getInstance();
        user1 = FirebaseAuth.getInstance().getCurrentUser();
        chip11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Action Completed", Toast.LENGTH_SHORT).show();
                TextView name =(TextView) findViewById(R.id.editTextTextPersonName10); //name
                TextView phone =(TextView) findViewById(R.id.editTextPhone2);  //phone
                TextView icnum =(TextView) findViewById(R.id.editTextNumber3);  //icnum
                TextView DOB =(TextView) findViewById(R.id.editTextTextPersonName5); //date of birth
                TextView gender =(TextView) findViewById(R.id.editTextTextPersonName7); //gender
                TextView age =(TextView) findViewById(R.id.editTextNumber6); //age
                //TextView address =(TextView) findViewById(R.id.editTextTextPersonName8); //address
                EditText email =(EditText) findViewById(R.id.editTextTextEmailAddress); //email
                EditText password = (EditText) findViewById(R.id.editTextTextPassword3); //password
                TextView conpass = (TextView)findViewById(R.id.editTextTextPassword5); //confirm password
                TextView deviceid = (TextView) findViewById(R.id.deviceid); //device id

                String namedb = name.getText().toString().trim();
                String phonedb = phone.getText().toString().trim();
                String icdb = icnum.getText().toString().trim();
                String dobdb = DOB.getText().toString().trim();
                String grnderdb = gender.getText().toString().trim();
                String agedb = age.getText().toString().trim();
               // String addressdb = address.getText().toString().trim();
                String emaildb = email.getText().toString().trim();
                String passdb = password.getText().toString().trim();
                String confpassdb = conpass.getText().toString().trim();
                String devicedb = deviceid.getText().toString().trim();

                userData.setUsername(namedb);
                userData.setDob(dobdb);
                userData.setGender(grnderdb);
               // userData.setAddress(addressdb);
                userData.setAge(agedb);
                userData.setPhone(phonedb);
                userData.setIc(icdb);
                userData.setRole(roledb);
                userData.store(namedb);
                userData.setEmail(emaildb);
                userData.setPassword(passdb);
                userData.setDevice(devicedb);
                readingData.setDevice(devicedb);

//                databaseReference.child(namedb).setValue(userData);
//                opensuccessRegristrationPage();

                firebaseAuth.createUserWithEmailAndPassword(emaildb,passdb).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            //databaseReference.child(namedb).setValue(userData);
                            databaseReference.child(user1.getUid()).setValue(userData);
                            Toast.makeText(signuppage.this, "User Created", Toast.LENGTH_SHORT).show();
                            opensuccessRegristrationPage();
                        } else {
                            Toast.makeText(signuppage.this, "Oh No", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
                public void openMainActivity () {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
                public void openprofilepage () {
                Intent intent = new Intent(this, profilepage.class);
                startActivity(intent);
            }
                public void opensuccessRegristrationPage () {
                    Intent intent = new Intent(this, successRegristrationPage.class);
                    startActivity(intent);
                }


            }