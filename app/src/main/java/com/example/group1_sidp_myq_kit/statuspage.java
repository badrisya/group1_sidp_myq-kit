package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.*;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class statuspage extends AppCompatActivity implements View.OnClickListener {
private DatabaseReference datareferencedayleft, dayleft, datareferencevalue,value;
private FirebaseAuth mAuth;
private String currentUserId;
private int countday = 0;
    private TextView note;
    private TextView note2;
    private TextView note3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statuspage);
        Button button13 = findViewById(R.id.button13);
        Button button14 = findViewById(R.id.button14);
        Button button15 = findViewById(R.id.button15);
        Button button16 = findViewById(R.id.button16);
        Button button25 = findViewById(R.id.button25);
        Button button2  = findViewById(R.id.button2);

        button13.setOnClickListener(this);
        button14.setOnClickListener(this);
        button15.setOnClickListener(this);
        button16.setOnClickListener(this);
        button25.setOnClickListener(this);
        button2.setOnClickListener(this);

      //  Calendar calendar = Calendar .getInstance();
      //  String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());
        TextView textviewdate = findViewById(R.id.dayleft);
        TextView address =(TextView) findViewById(R.id.address); //address
        //textviewdate.setText(currentDate);
        //int i =14;
        mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        datareferencedayleft = FirebaseDatabase.getInstance().getReference("UserData");
        datareferencevalue = FirebaseDatabase.getInstance().getReference("UserData");
        datareferencedayleft.child(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);
                ReadingData readingData = snapshot.getValue(ReadingData.class);
                if (userData !=null){

                    dayleft = FirebaseDatabase.getInstance().getReference().child("ReadingData").child("Device1");
                    dayleft.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                int i = 14;
                                int j ;
                                countday = (int) snapshot.getChildrenCount();
                                j = i-countday;
                                if(countday <=i){
                                    textviewdate.setText(Integer.toString(j) +"days");

                                }
                                //textviewdate.setText(Integer.toString(countday) +"days");

                            }
                            else
                            {//countday = (int) snapshot.getChildrenCount();
                               // textviewdate.setText(Integer.toString(countday) +"days");
                                 textviewdate.setText("0");
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull @NotNull DatabaseError error) {

                        }
                    });
                }
            }


            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        //start new reference

        datareferencevalue.child(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);
                if (userData != null){

                    String adddb = userData.getAddress();
                    address.setText(adddb);

                }

                if (userData != null){

                    value = FirebaseDatabase.getInstance().getReference("ReadingData").child(userData.getDevice());
                    value.addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                ReadingData readingData = dataSnapshot.getValue(ReadingData.class);

                                if (readingData != null) {

                                    double hrdb = readingData.getHeartrate();
                                    double spo = readingData.getSpo2();
                                    double temp = readingData.getTemp();
                                    note = (TextView) findViewById(R.id.temp); //vaccinepatient riskpatient
                                    note2 = (TextView) findViewById(R.id.heartrate);
                                    note3 = (TextView) findViewById(R.id.spo2);
                                    DecimalFormat form = new DecimalFormat("0.0");
                                    note.setText(  form.format(temp) );
                                    note2.setText(  new DecimalFormat("#").format(hrdb) );
                                    note3.setText( form.format(spo) );
                                }




                            }





                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(statuspage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }


                    });


                }


            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                Toast.makeText(statuspage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();

            }
        });

        //dayleft = FirebaseDatabase.getInstance().getReference().child("ReadingData").child("Device1");



    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button13:
                ;
                openstatuspage();
                break;
            case R.id.button14:
                ;
                //openstatusmainpage();
                openhistorypage();
                break;
            case R.id.button15:
                ;
                openNoticationpageActivity();
                break;
            case R.id.button16:
                ;
                openprofilepage();
                break;
            case R.id.button25:
                ;
                openmapdetailspage();
                break;
            case R.id.button2:
                ;
                openstatuspage();
                break;

        }
    }



        public void openNoticationpageActivity(){
            Intent intent = new Intent(this,ChatActivity2.class);
            startActivity(intent);
        }
        public void openprofilepage() {
            Intent intent = new Intent(this, profilepage.class);
            startActivity(intent);
        }
    public void openstatuspage() {
        Intent intent = new Intent(this, statuspage.class);
        startActivity(intent);
    }
    public void openaccountloginpage(){
        Intent intent = new Intent(this,accountloginpage.class);
        startActivity(intent);
    }

    public void openstatusmainpage()
    {
        Intent intent= new Intent(this,Statusmainpage.class);
        startActivity(intent);
    }

    public void openmapdetailspage()
    {
        Intent intent= new Intent(this,maps.class);
        startActivity(intent);
    }

    public void openhistorypage(){
        Intent intent = new Intent(this,History2.class);
        startActivity(intent);
    }
}
