package com.example.group1_sidp_myq_kit;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;

import static androidx.core.app.NotificationCompat.*;

public class profilepage extends AppCompatActivity implements View.OnClickListener{
    //private Button buttonbottom;
    DatabaseReference databaseReference, dbref2;;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    public static final String CHANNEL_1 = "channel1";
    private NotificationManagerCompat notificationManager;

    String userid;

  Double heartrate = 120.0 ;
  Double temperature= 37.5;
  Double spo2=94.0;
    private TextView Temp;
    private TextView spo;
    private TextView note;
    private TextView note2;
    private TextView note3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilepage);

        Button button9 = findViewById(R.id.button9);
        Button button10 = findViewById(R.id.button10);
        Button button11 = findViewById(R.id.button11);
        Button button12 = findViewById(R.id.button12);
       // Button button30 = findViewById(R.id.button30);
        button9.setOnClickListener(this);
        button10.setOnClickListener(this);
        button11.setOnClickListener(this);
        button12.setOnClickListener(this);
       // button30.setOnClickListener(this);

        TextView name =(TextView) findViewById(R.id.patientname); //name
        TextView phone =(TextView) findViewById(R.id.textView3);  //phone
        TextView icnum =(TextView) findViewById(R.id.textView2);  //icnum
//        TextView DOB =(TextView) findViewById(R.id.editTextTextPersonName5); //date of birth
//        TextView gender =(TextView) findViewById(R.id.editTextTextPersonName7); //gender
//        TextView age =(TextView) findViewById(R.id.editTextNumber6); //age
        TextView address =(TextView) findViewById(R.id.textView8); //address

        user = FirebaseAuth.getInstance().getCurrentUser();
        userid = user.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("UserData");
        notificationManager = NotificationManagerCompat.from(this);
        //Temp part








        // Temp = FirebaseDatabase.getInstance().getReference().child("ReadingData").child("Device1"); //read device temp and spo2

        databaseReference.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);

                if (userData != null){
                    dbref2 = FirebaseDatabase.getInstance().getReference("ReadingData").child(userData.getDevice());
                    dbref2.addListenerForSingleValueEvent(new ValueEventListener() {
                        
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                ReadingData readingData = dataSnapshot.getValue(ReadingData.class);

                                if (readingData != null) {
                                    double hrdb = readingData.getHeartrate();
                                    double spo = readingData.getSpo2();
                                    double temp = readingData.getTemp();
                                    //nt vaccin =1;
                                    note = (TextView) findViewById(R.id.textView5); //vaccinepatient riskpatient
                                    note2 = (TextView) findViewById(R.id.textView10);
                                    note3 = (TextView) findViewById(R.id.textView30);
                                    Temp = (TextView) findViewById(R.id.riskpatient);
                                    //createTextView(String.valueOf(hrdb),String.valueOf(spo),String.valueOf(temp));
                                    if (hrdb <= heartrate && spo >= spo2) {
                                        if (temp <= temperature) {
                                            Temp.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.green));
                                            Temp.setText("Low Risk");

                                            DecimalFormat form = new DecimalFormat("0.0");

                                            note.setText("Temperature : " + form.format(temp) );
                                            note2.setText("Heart rate :" + new DecimalFormat("#").format(hrdb) );
                                            note3.setText("SPO2 :" + form.format(spo) );



                                            createNotification();



                                        } else {
                                            Temp.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.red));
                                            Temp.setText("High risk");
                                           // notificationManager.notify(999,builder.build());



                                        }
                                        //findViewById(R.layout.item_notiuser_row);

                                    }
                                }
                                }


                            }


                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(profilepage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(profilepage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

     /*   vaccine = (TextView) findViewById(R.id.vaccinepatient); //vaccinepatient riskpatient
        Temp = (TextView)findViewById(R.id.riskpatient);
        double  temp = 36.5;
        double spo2 =95;
        int vaccin= 1;

        if(temp < 36.5 || spo2 <=95  )  //assume temp,spo2 and vaccinated state is high risk and vacinatted
        {
            if(vaccin == 1) {
                vaccine.setText(" Vaccinated ");
                vaccine.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.green));
            }else{
                vaccine.setText(" Unvaccinated ");
                vaccine.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.red));
            }
            //findViewById(R.layout.item_notiuser_row);
            Temp.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.green));

            // holder.text.setTextColor(R.color.Red);
            Intent notificationintent = new Intent(this,HomeActivity.class);  //this means this page ,go to next page
            notificationintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationintent,PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"lemubitA")
                    .setSmallIcon((R.drawable.ic_android_black_24dp))
                    .setContentTitle("Doctor Notification")
                    .setContentText(" Patient in high risk.. abnormal sysmptoms")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            NotificationManagerCompat notificationManager =NotificationManagerCompat.from(this);

            Temp.setText("High Risk ");

            notificationManager.notify(999,builder.build());


        }else if (temp <= 36.5 && spo2>=95 )
        {
            Temp.setBackgroundColor(ContextCompat.getColor(Temp.getContext(), R.color.green));
            Temp.setText("Low Risk ");
            // notificationManager.notify(999,builder.build());

        }
        else if (vaccin == 1){
            vaccine.setBackgroundColor(ContextCompat.getColor(vaccine.getContext(), R.color.green));
            vaccine.setText("High Risk ");

        }


        //above are temp till here
*/
        databaseReference.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);

                if (userData != null){
                    String namedb = userData.getUsername();
                    String phonedb = userData.getPhone();
                    String icdb = userData.getIc();
                    String adddb = userData.getAddress();

                    name.setText(namedb);
                    phone.setText(phonedb);
                    icnum.setText(icdb);
                    address.setText(adddb);

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(profilepage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }
    //button11.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button9: ;

                openstatuspage();
                break;
            case R.id.button10: ;
                //openstatusmainpage();
                openhistorypage();
                break;
            case R.id.button11:
                openChatActivityReceive();

                break;
            case R.id.button12:
                openprofilepage();
                break;




        }
    }


    //  });
 /*public void sendChannel1(View v){
     Notification notification = new NotificationCompat.Builder(this,CHANNEL_1)
             .setSmallIcon((R.drawable.ic_android_black_24dp))
             .setContentTitle(" Notification from ")
             .setContentText(" symptoms warning ...")
             .setPriority(NotificationCompat.PRIORITY_HIGH)
             .build();
     notificationManager.notify(1,notification);


 }*/
    private void createNotification(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel1 = new NotificationChannel(CHANNEL_1, "Channel 1",NotificationManager.IMPORTANCE_DEFAULT);
            channel1.setDescription("This is blabla");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);

            Intent resultIntent = new Intent(this,doctorlogin.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
            Notification notification = new NotificationCompat.Builder(this,CHANNEL_1)
                    .setSmallIcon((R.drawable.ic_android_black_24dp))
                    .setContentTitle(" Notification from patient ")
                    .setContentText(" Check patient vitals sign" )
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)

                    .setContentIntent(resultPendingIntent)
                    .build();
                    notificationManager.notify(1,notification);

        }
    }




/*private void notification(){
        Intent notificationintent = new Intent(this,profilepage.class);  //this means this page ,go to next page
    notificationintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationintent,PendingIntent.FLAG_UPDATE_CURRENT);
    Builder builder = new Builder(this,"testapp")
            .setSmallIcon((R.drawable.ic_android_black_24dp))
            .setContentTitle(" Notification from ")
            .setContentText(" symptoms warning ...")
            .setPriority(PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true);
    NotificationManagerCompat notificationManager =NotificationManagerCompat.from(this);
    notificationManager.notify(999,builder.build());

}*/





    public void openprofilepage() {
        Intent intent = new Intent(this, profilepage.class);

        startActivity(intent);
    }
    public void openstatuspage() {
        Intent intent = new Intent(this, statuspage.class);
        startActivity(intent);
    }
    public void openeditprofilepage(){
        Intent intent = new Intent(this,edit_profilepage.class);
        startActivity(intent);
    }
    public void openhomeactivity()
    {
        Intent intent= new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
    public void openChatActivityReceive(){
        Intent intent = new Intent(this,ChatActivity2.class);
        startActivity(intent);
    }
    public void openstatusmainpage()
    {
        Intent intent= new Intent(this,Statusmainpage.class);
        startActivity(intent);
    }

    public void openhistorypage(){
        Intent intent = new Intent(this,History2.class);
        startActivity(intent);
    }

}
