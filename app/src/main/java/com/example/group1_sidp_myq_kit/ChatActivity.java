//package com.example.group1_sidp_myq_kit;
//
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Intent;
//import android.os.Build;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import android.os.Bundle;
//import androidx.cardview.widget.CardView;
//import androidx.core.app.NotificationCompat;
//import androidx.core.app.NotificationManagerCompat;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.*;
//import org.jetbrains.annotations.NotNull;
//
//import java.util.ArrayList;
//import java.util.Date;
//
//public class ChatActivity extends AppCompatActivity {
//
//    String ReciverUID,ReciverName,SenderUID;
//    TextView reciverName;
//    FirebaseDatabase database;
//    FirebaseAuth firebaseAuth;
//
//    CardView sendBtn;
//    EditText edtMessage;
//    EditText editMessage2;
//
//    String senderRoom, reciverRoom;
//    RecyclerView messageAdater;
//    ArrayList<Messages> messagesArrayList;
//
//    MessageAdater adater;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_chat3);
//
//        database=FirebaseDatabase.getInstance();
//        firebaseAuth=FirebaseAuth.getInstance();
//
//        ReciverName=getIntent().getStringExtra("username");
//        ReciverUID=getIntent().getStringExtra("uid");
//
//        messagesArrayList= new ArrayList<>();
//
//        reciverName=findViewById(R.id.reciverName);
//
//        messageAdater = findViewById(R.id.messageAdater);
//        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this);
//        linearLayoutManager.setStackFromEnd(true);
//        messageAdater.setLayoutManager(linearLayoutManager);
//        adater= new MessageAdater(ChatActivity.this,messagesArrayList);
//        messageAdater.setAdapter(adater);
//
//        sendBtn=findViewById(R.id.sendBtn);
//        edtMessage=findViewById(R.id.edtMessage);
//
//
//        reciverName.setText(""+ReciverName); //reciverName textviewid
//
//        SenderUID=firebaseAuth.getUid();
//
//         senderRoom= SenderUID+ReciverUID;
//        reciverRoom= ReciverUID+SenderUID;
//
//
//
//        String message=edtMessage.getText().toString();
//
//
//        DatabaseReference reference=database.getReference().child("UserData").child(firebaseAuth.getUid());
//        DatabaseReference chatRefrece = database.getReference().child("chats").child(senderRoom).child("messages");
//
//        Intent notificationintent = new Intent(this,accountloginpage.class);  //go to next page
//        notificationintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationintent,PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"lemubitA")
//                .setSmallIcon((R.drawable.ic_android_black_24dp))
//                .setContentTitle("Patient Notification")
//                .setContentText(" Messages from Doctor ...")
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setContentIntent(pendingIntent)
//                .setAutoCancel(true);
//
//        NotificationManagerCompat notificationManager =NotificationManagerCompat.from(this);
//       // notification();
//
//        chatRefrece.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
//                messagesArrayList.clear();
//                for(DataSnapshot dataSnapshot :snapshot.getChildren())
//                {
//                    Messages messages=dataSnapshot.getValue(Messages.class);
//                    messagesArrayList.add(messages);
//
//                }
//                adater.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(@NonNull @NotNull DatabaseError error) {
//
//            }
//        });
//
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
//
//                snapshot.child("email").getValue().toString();
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull @NotNull DatabaseError error) {
//
//            }
//        });
//
//
//        sendBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String message=edtMessage.getText().toString();
//
//
//                if(message.isEmpty())
//                {
//                    Toast.makeText(ChatActivity.this,"Please Enter Valid Message",Toast.LENGTH_SHORT).show();
//                    return;
//
//                }
//                edtMessage.setText("");
//                Date date = new Date();
//                notificationManager.notify(999,builder.build());
//
//                Messages messages=new Messages(message,SenderUID,date.getTime());
//
//                database=FirebaseDatabase.getInstance();
//                database.getReference().child("chats")
//                        .child(senderRoom)
//                        .child("messages")
//                        .setValue(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull @NotNull Task<Void> task) {
//                                database.getReference().child("chats")
//                                        .child(reciverRoom)
//                                        .child("messages")
//                                        .setValue(messages).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                            @Override
//                                            public void onComplete(@NonNull @NotNull Task<Void> task) {
//
//                                            }
//                                        });
//
//
//
//
//                            }
//                        });
//
//
//
//            }
//        });
//
//
//
//
//    }
//  /*  private void notification (){
//
//        String message = edtMessage.getText().toString();
//
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            NotificationChannel channel =
//                    new NotificationChannel("n","n", NotificationManager.IMPORTANCE_DEFAULT);
//
//            NotificationManager manager = getSystemService(NotificationManager.class);
//            manager.createNotificationChannel(channel);
//
//        }
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "n")
//                .setSmallIcon((R.drawable.ic_android_black_24dp))
//                .setContentTitle("Doctor Notification")
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setAutoCancel(true)
//                .setContentText(message +"hello ");
//
//        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
//        managerCompat.notify(999,builder.build());
//    }*/
//}