package com.example.group1_sidp_myq_kit;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class doctorprofilepage extends AppCompatActivity implements View.OnClickListener {
    DatabaseReference databaseReference, dbref2;;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;

    String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctorprofilepage);

        Button button4 = findViewById(R.id.button4);
        Button button19 = findViewById(R.id.button19);
        Button button17 = findViewById(R.id.button17);

        button4.setOnClickListener(this);  //patient list
        button17.setOnClickListener(this);  //notification
        button19.setOnClickListener(this);  //doc profile


        TextView name = (TextView) findViewById(R.id.docname); //name
        TextView phone = (TextView) findViewById(R.id.phonenumdoc);  //phone
        TextView icnum = (TextView) findViewById(R.id.icdoc);
        TextView email = (TextView) findViewById(R.id.email);
        TextView specialist = (TextView) findViewById(R.id.specialist);
        TextView worklocation = (TextView) findViewById(R.id.worklocation);

        user = FirebaseAuth.getInstance().getCurrentUser();
        userid = user.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("UserData");

        databaseReference.child(userid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserData userData = snapshot.getValue(UserData.class);

                if (userData != null) {

                    String namedb = userData.getUsername();
                    String phonedb =userData.getPhone();
                    String icdb = userData.getIc();
                    String docadddb = userData.getAddress();
                   String docemaildb = userData.getEmail();
                    String docspecialistdb = userData.getSpecialist();
                   String docworklocationdb = userData.getWorkingPlace();

                    name.setText(namedb);
                    phone.setText(phonedb);
                    icnum.setText(icdb);
                    //address.setText(adddb);
                    email.setText(docemaildb);
                    specialist.setText(docspecialistdb);
                    worklocation.setText(docworklocationdb);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(doctorprofilepage.this, "Oops! Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button4: ;
               openDrviewpatient();// openhomeactivity(); // change to high low risk patient list
                break;

            case R.id.button17:
                openhomeactivity();
                break;

            case R.id.button19: ;
                opendoctorprofilepage();
                break;


        }


    }








    public void opendoctorprofilepage() {
        Intent intent = new Intent(this, doctorprofilepage.class);
        startActivity(intent);
    }

    public void openhomeactivity(){
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }

    public void openDrviewpatient()
    {
        Intent intent = new Intent(this,drviewpatientprofile.class);
        startActivity(intent);
    }
}