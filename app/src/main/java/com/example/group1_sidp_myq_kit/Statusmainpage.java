package com.example.group1_sidp_myq_kit;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class Statusmainpage extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statusmainpage);
        
        Button button21 = findViewById(R.id.button21);
        button21.setOnClickListener(this);
    }

    public void onClick(View view){
        opensymptomspage();

    }
    public void opensymptomspage()
    {
        Intent intent= new Intent(this,symptomspage.class);
        startActivity(intent);
    }
}